package com.example.daniel.HR_App;

import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

/**
 * Created by Daniel on 28/01/2018.
 */

public class TermsListFragement extends Fragment {

    ListView listViewTerms;
    DbManager dbManager;
    Cursor cursor;
    SimpleCursorAdapter adapter;
    String from [] = {"_id","keyword"};
    int to [] = {R.id.txtIdTerms ,R.id.txtWordTerms};
    String searched;
    LinearLayout termsListContainer;
    MainActivity activity;
    TextView txtTermListQueryResult;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_list_terms, container, false);

        listViewTerms = (ListView)rootView.findViewById(R.id.listViewTerms);
        termsListContainer = (LinearLayout) rootView.findViewById(R.id.termsListContainer);
        txtTermListQueryResult = rootView.findViewById(R.id.txtTermListQueryResult);

        dbManager = new DbManager(getActivity());
        dbManager.open();

        activity = (MainActivity)getActivity();
        searched = activity.search;

        if (searched.equals("")){
            cursor = dbManager.fetchTags();
            checkResults(cursor);
        }
        else {
            cursor = dbManager.filteredFetchTerms(searched);
            checkResults(cursor);
        }

        adapter = new SimpleCursorAdapter(getActivity(), R.layout.list_terms, cursor, from, to, 0);
        adapter.notifyDataSetChanged();
        listViewTerms.setAdapter(adapter);
        listViewTerms.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                TextView t1 = view.findViewById(R.id.txtWordTerms);
                TextView t2 = view.findViewById(R.id.txtIdTerms);

                String term = t1.getText().toString();
                String id = t2.getText().toString();

                Bundle bundle = new Bundle();
                bundle.putString("term",term);
                bundle.putString("id",id);
                bundle.putString("search","no");
                bundle.putBoolean("fromSearcher",false);

                Fragment fragment1 = new TermViewerFragment();

                activity.fragment = fragment1;
                activity.prevFragment = new TermsListFragement();
                activity.changeDrawerToBackButton();

                fragment1.setArguments(bundle);
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right,android.R.anim.slide_in_left,android.R.anim.slide_out_right );
                transaction.replace(R.id.frameLayout, fragment1).commit();
            }
        });

        return rootView;
    }
    public void checkResults(Cursor cursor){
        if (searched.equals("") && !activity.showAllValuesTerms){
            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)termsListContainer.getLayoutParams();
            params.height = 0;
            termsListContainer.setLayoutParams(params);
            txtTermListQueryResult.setText("Start typing to search");
        }
        else {
            if (cursor.getCount() == 0){
                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)termsListContainer.getLayoutParams();
                params.height = 0;
                termsListContainer.setLayoutParams(params);
                txtTermListQueryResult.setText("No match found");
            }
            else {
                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)termsListContainer.getLayoutParams();
                params.height = ViewGroup.LayoutParams.MATCH_PARENT;
                termsListContainer.setLayoutParams(params);
            }
        }
    }
}
