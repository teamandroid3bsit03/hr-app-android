package com.example.daniel.HR_App;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class TermsAndAgreementActivity extends AppCompatActivity {

    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_and_agreement);

        sharedPreferences = getApplicationContext().getSharedPreferences("SETTINGS", Context.MODE_PRIVATE);

        Button btnAgree = (Button)findViewById(R.id.btnAgree);

        btnAgree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent view = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(view);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("FIRST_TIME", "0").commit();
                finish();
            }
        });
    }
}
