package com.example.daniel.HR_App;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Daniel on 20/01/2018.
 */

public class DbHelper extends SQLiteOpenHelper {
    public static final String DBNAME= "dbTest";
    public static final int VERSION = 1;

    public static final String TITLE="title";
    public static final String id="_id";
    public static final String DATE="date";
    public static final String CONTENT="content";
    String[][] topics = {
            {"Checklist Guide for Effective Feedback","0","0"},
            {"Classroom Assessment Strategies","0","1"},
            {"Formative Assessment","0","2"},
            {"Providing Effective Feedback","0","3"},
            {"Sample CATs","0","4"},
            {"Student Self-Assessment","0","5"},

            {"Communication Confident Authority","1","0"},
            {"Cooperative Reading Groups","1","1"},
            {"Cushioning Questions","1","2"},
            {"Learning Sheets","1","3"},
            {"Reading for Task","1","4"},
            {"Risk Language","1","5"},
            {"Setting Procedures and Expectations","1","6"},
            {"Truth Signs","1","7"},

            {"Checklist Guide to Sharing Learning Targets and Success Criteria","2","0"},
            {"Learning Target","2","1"},
            {"Performance Task","2","2"},
            {"Sharing Learning Targets and Success Criteria","2","3"},
            {"Success Criteria","2","4"},

            {"3-Step Interview","3","0"},
            {"Analytic Teams","3","1"},
            {"Buzz Groups","3","2"},
            {"Drill Review Pairs","3","3"},
            {"Face Off Game","3","4"},
            {"Guided Reciprocal Peer Questioning","3","5"},
            {"Jigsaw","3","6"},
            {"Learning Cell","3","7"},
            {"Mastery Learning Game","3","8"},
            {"Note-Taking Pairs","3","9"},
            {"Numbered Heads Together","3","10"},
            {"Question, All Write","3","11"},
            {"Questioning Strategies","3","12"},
            {"Questioning Strategies2","3","13"},
            {"Roundtable","3","14"},
            {"Selecting Group Size","3","15"},
            {"Selecting Members for Groups","3","16"},
            {"Send a Problem","3","17"},
            {"Structured Academic Controversies","3","18"},
            {"Structured Problem Solving","3","19"},
            {"Talking Chips","3","20"},
            {"Test Taking Teams","3","21"},
            {"Think Pair Share","3","22"},
            {"Think Pair Square","3","23"},
            {"Turn and Talk","3","24"},
            {"Underexplain and Learning Pairs","3","25"},
            {"Wait Time","3","26"},
            };
    String keyword_topics[][] = {
            {"1","Assessment"},
            {"1","Feedback"},
            {"1","Checklist"},

            {"2","Assessment"},
            {"2","Formative "},
            {"2","CATs"},

            {"3","Assessment"},
            {"3","Formative "},

            {"4","Assessment"},
            {"4","Feedback"},
            {"4","Formative"},

            {"5","Assessment"},
            {"5","CATs"},
            {"5","Assessment Techniques"},
            {"5","Formative"},
            {"5","Minute Paper"},
            {"5","Muddiest Point"},
            {"5","Chain Notes"},
            {"5","Memory Matrix"},
            {"5","Directed Paraphrasing"},
            {"5","One-Sentence summary"},
            {"5","Exam Evaluations"},
            {"5","Application Cards"},
            {"5","Listing"},

            {"6","Assessment"},
            {"6","Self-Assessment"},
            {"6","Formative"},

            {"7","Classroom Environment and Culture"},
            {"7","Classroom Management"},
            {"7","Teacher Authority"},

            {"8","Curriculum and Pedagogy"},
            {"8","Cooperative Learning"},
            {"8","Reading"},

            {"9","Classroom Environment and Culture"},
            {"9","Effective Questioning"},
            {"9","Cushioning"},

            {"10","Curriculum and Pedagogy"},
            {"10","Individualized activity"},
            {"10","Reading"},
            {"10","Parallel Classes"},

            {"11","Curriculum and Pedagogy"},
            {"11","Individualized activity"},
            {"11","Reading"},
            {"11","Parallel Classes"},

            {"12","Classroom Environment and Culture"},
            {"12","Effective Questioning"},
            {"12","Encouraging Response"},

            {"13","Classroom Environment and Culture"},
            {"13","Classroom Management"},
            {"13","Rules and Procedures"},
            {"13","Order"},

            {"14","Classroom Environment and Culture"},
            {"14","Motivation"},
            {"14","Student Effort"},

            {"15","Purpose"},
            {"15","Learning Target"},
            {"15","Success Criteria"},
            {"15","Checklist"},

            {"16","Purpose"},
            {"16","Learning Target"},

            {"17","Purpose"},
            {"17","Learning Target"},
            {"17","Performance Task"},

            {"18","Purpose"},
            {"18","Learning Target"},
            {"18","Success Criteria"},
            {"18","Technique"},

            {"19","Purpose"},
            {"19","Learning Target"},
            {"19","Success Criteria"},

            {"20","Student Engagement"},
            {"20","Pair Learning"},
            {"20","Parallel Classes"},

            {"21","Student Engagement"},
            {"21","Group Work"},
            {"21","Parallel Classes"},

            {"22","Student Engagement"},
            {"22","Group Work"},
            {"22","Brainstorming"},

            {"23","Student Engagement"},
            {"23","Pair Learning"},
            {"23","Deliberate Practice"},
            {"23","Parallel Classes"},

            {"24","Student Engagement"},
            {"24","Games"},
            {"24","Deliberate Practice"},

            {"25","Student Engagement"},
            {"25","Pair Learning"},
            {"25","Questioning"},
            {"25","Parallel Classes"},

            {"26","Student Engagement"},
            {"26","Group Work"},
            {"26","Independent Learning"},
            {"26","Parallel Classes"},

            {"27","Student Engagement"},
            {"27","Pair Learning"},
            {"27","Deliberate Practice"},
            {"27","Parallel Classes"},

            {"28","Student Engagement"},
            {"28","Games"},
            {"28","Deliberate Practice"},

            {"29","Student Engagement"},
            {"29","Pair Learning"},
            {"29","Notes"},

            {"30","Student Engagement"},
            {"30","Group Work"},

            {"31","Student Engagement"},
            {"31","Questioning"},
            {"31","Individualized activity"},
            {"31","Parallel Classes"},

            {"32","Student Engagement"},
            {"32","Questioning"},

            {"33","Student Engagement"},
            {"33","Questioning"},

            {"34","Student Engagement"},
            {"34","Group Work"},
            {"34","Brainstorming"},
            {"34","Parallel Classes"},

            {"35","Student Engagement"},
            {"35","Group Work"},
            {"35","Group Size"},

            {"36","Student Engagement"},
            {"36","Group Work"},
            {"36","Group Size"},

            {"37","Student Engagement"},
            {"37","Group Work"},
            {"37","Deliberate Practice"},
            {"37","Brainstorming"},

            {"38","Student Engagement"},
            {"38","Debate"},
            {"38","Critical Thinking"},

            {"39","Student Engagement"},
            {"39","Group Work"},
            {"39","Problem Solving"},
            {"39","Parallel Classes"},

            {"40","Student Engagement"},
            {"40","Communication"},
            {"40","Brainstorming"},
            {"40","Discussion"},

            {"41","Student Engagement"},
            {"41","Testing"},
            {"41","Deliberate Practice"},
            {"41","Group Work"},

            {"42","Student Engagement"},
            {"42","Pair Learning"},
            {"42","Brainstorming"},
            {"42","Parallel Classes"},

            {"43","Student Engagement"},
            {"43","Pair Learning"},
            {"43","Brainstorming"},

            {"44","Student Engagement"},
            {"44","Pair Learning"},
            {"44","Brainstorming"},

            {"45","Student Engagement"},
            {"45","Pair Learning"},
            {"45","Content Delivery"},
            {"45","Parallel Classes"},

            {"46","Student Engagement"},
            {"46","Questioning"},
    };
    public DbHelper(Context context) {
        super(context, DBNAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("CREATE TABLE notes(" + id + " INTEGER PRIMARY KEY AUTOINCREMENT, " + TITLE + " TEXT NOT NULL, " + DATE+ " TEXT NOT NULL, " + CONTENT+ " TEXT NOT NULL);");

        sqLiteDatabase.execSQL("CREATE TABLE topics(" + id + " INTEGER PRIMARY KEY AUTOINCREMENT, topic TEXT NOT NULL, groupno TEXT NOT NULL, itemno TEXT NOT NULL);");
        sqLiteDatabase.execSQL("CREATE TABLE keywordtopics(" + id + " INTEGER PRIMARY KEY AUTOINCREMENT, topicid TEXT NOT NULL, keyword TEXT NOT NULL);");

        insertTopicValues("topics (topic, groupno, itemno)", topics, sqLiteDatabase);
        insertDefaultValues("keywordtopics (topicid, keyword)", keyword_topics, sqLiteDatabase);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS notes");
        onCreate(sqLiteDatabase);
    }
    public void insertDefaultValues(String tableNameWithColumn,String contents[][],SQLiteDatabase sqLiteDatabase1){
        for (int i = 0; i < contents.length; i++){
            sqLiteDatabase1.execSQL("INSERT INTO " + tableNameWithColumn +" VALUES ('"+ contents[i][0] + "', '" + contents[i][1] +"');");
        }
    }
    public void insertTopicValues(String tableNameWithColumn,String contents[][],SQLiteDatabase sqLiteDatabase1){
        for (int i = 0; i < contents.length; i++){
            sqLiteDatabase1.execSQL("INSERT INTO " + tableNameWithColumn +" VALUES ('"+ contents[i][0] + "', '" + contents[i][1] + "', '" + contents[i][2] + "');");
        }
    }
}
