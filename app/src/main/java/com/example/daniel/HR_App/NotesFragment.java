package com.example.daniel.HR_App;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Daniel on 22/01/2018.
 */

public class NotesFragment extends Fragment {

    ListView listView;
    DbManager dbManager;
    Cursor cursor, cursorList;
    SimpleCursorAdapter adapter;
    String from [] = {"_id","title","date","content"};
    int to [] = {R.id.txtId ,R.id.txtTitle, R.id.txtdate, R.id.txtContent};
    String contextSelected;
    LinearLayout notesListContainer;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_note, container, false);

        listView = (ListView)rootView.findViewById(R.id.listViewNotes);
        notesListContainer = (LinearLayout) rootView.findViewById(R.id.notesListContainer);

        dbManager = new DbManager(getActivity());
        dbManager.open();

        cursor = dbManager.fetchNote("notes",from);
        checkResults(cursor);
        adapter = new SimpleCursorAdapter(getActivity(), R.layout.list_notes, cursor, from, to, 0);
        adapter.notifyDataSetChanged();
        listView.setAdapter(adapter);
        registerForContextMenu(listView);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                TextView t1 = view.findViewById(R.id.txtTitle);
                TextView t2 = view.findViewById(R.id.txtContent);
                TextView t3 = view.findViewById(R.id.txtId);

                String title = t1.getText().toString();
                String content = t2.getText().toString();
                String id = t3.getText().toString();

                Intent intent = new Intent(getActivity(), AddNoteActivity.class);
                intent.putExtra("title", title);
                intent.putExtra("content", content);
                intent.putExtra("id", ""+ id);
                intent.putExtra("switch", "0");
                startActivity(intent);
            }
        });

        return rootView;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        MenuInflater inflater = getActivity().getMenuInflater();
        inflater.inflate(R.menu.context_menu, menu);
        if (v.getId() == R.id.listViewNotes){
            ListView lv = (ListView)v;
            AdapterView.AdapterContextMenuInfo amci = (AdapterView.AdapterContextMenuInfo) menuInfo;
            cursorList = (Cursor) lv.getItemAtPosition(amci.position);
            contextSelected = cursorList.getString(cursor.getColumnIndex("_id"));
        }
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.contextDelete:
                dbManager.delete(contextSelected);
                resetListView();
                return  true;
            case R.id.contextEdit:
                Intent intent = new Intent(getActivity(), AddNoteActivity.class);
                intent.putExtra("title", cursorList.getString(cursor.getColumnIndex("title")));
                intent.putExtra("content", cursorList.getString(cursor.getColumnIndex("content")));
                intent.putExtra("id", ""+ cursorList.getString(cursor.getColumnIndex("_id")));
                intent.putExtra("switch", "2");
                startActivity(intent);
                return  true;
            default:
                return false;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.item_add){
            Intent intents = new Intent(getContext(), AddNoteActivity.class);
            intents.putExtra("switch", "1");
            startActivity(intents);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        resetListView();
    }

    public void resetListView(){
        dbManager = new DbManager(getActivity());
        dbManager.open();
        cursor = dbManager.fetchNote("notes",from);
        checkResults(cursor);
        adapter = new SimpleCursorAdapter(getActivity(), R.layout.list_notes, cursor, from, to, 0);
        adapter.notifyDataSetChanged();
        listView.setAdapter(adapter);
    }
    public void checkResults(Cursor cursor){
        if (cursor.getCount() == 0){
            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)notesListContainer.getLayoutParams();
            params.height = 0;
            notesListContainer.setLayoutParams(params);
        }
        else {
            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)notesListContainer.getLayoutParams();
            params.height = ViewGroup.LayoutParams.MATCH_PARENT;
            notesListContainer.setLayoutParams(params);
        }
    }
}
