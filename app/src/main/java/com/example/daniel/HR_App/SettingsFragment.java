package com.example.daniel.HR_App;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Daniel on 30/01/2018.
 */

public class SettingsFragment extends Fragment {

    Button btnSettingsApply, btnSettingsCancel;
    Switch switchDefualt;
    TextView txtSettingsTitle, txtSettingsHeader, txtSettingsBody;
    SeekBar seekBarSettings;
    SharedPreferences sharedPreferences;
    Preferences preferences;
    String def;
    int fontSize;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_settings, container, false);
//wip
        btnSettingsApply = rootView.findViewById(R.id.btnSettingsApply);
        btnSettingsCancel = rootView.findViewById(R.id.btnSettingsCancel);
        switchDefualt = rootView.findViewById(R.id.switchDefault);
        txtSettingsBody =  rootView.findViewById(R.id.txtSettingsBody);
        txtSettingsTitle =  rootView.findViewById(R.id.txtSettingsTitle);
        txtSettingsHeader =  rootView.findViewById(R.id.txtSettingsHeader);
        seekBarSettings =  rootView.findViewById(R.id.seekBarSettings);
        seekBarSettings.setEnabled(false);

        preferences = new Preferences(getActivity());

        sharedPreferences = getActivity().getSharedPreferences("SETTINGS", Context.MODE_PRIVATE);
        def = sharedPreferences.getString("DEFAULT","0");
        fontSize = sharedPreferences.getInt("FONT_SIZE",5);
        changeFontSize(fontSize);
        seekBarSettings.setProgress(fontSize);

        seekBarSettings.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                changeFontSize(progress);
                fontSize = progress;
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        switchDefualt.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    def = "1";
                    seekBarSettings.setEnabled(false);
                    btnSettingsApply.setEnabled(false);
                    btnSettingsCancel.setEnabled(false);
                    btnSettingsApply.setBackgroundColor(getActivity().getResources().getColor(R.color.disabled));
                    btnSettingsCancel.setBackgroundColor(getActivity().getResources().getColor(R.color.disabled));
                    txtSettingsBody.setTextColor(getResources().getColor(R.color.gray));
                    txtSettingsHeader.setTextColor(getResources().getColor(R.color.gray));
                    txtSettingsTitle.setTextColor(getResources().getColor(R.color.gray));
                    seekBarSettings.setProgress(5);
                    txtSettingsTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP,30);
                    txtSettingsHeader.setTextSize(TypedValue.COMPLEX_UNIT_SP,20);
                    txtSettingsBody.setTextSize(TypedValue.COMPLEX_UNIT_SP,15);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("DEFAULT",def);
                    editor.putInt("FONT_SIZE",5);
                    editor.commit();
                    preferences.setFontStyle(FontStyle.FontStyle5);
                    Intent intent = new Intent(getActivity(), MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intent);
                    getActivity().finish();
                    Toast.makeText(getActivity(), "Default text size applied", Toast.LENGTH_SHORT).show();
                }
                else {
                    def = "0";
                    seekBarSettings.setEnabled(true);
                    btnSettingsApply.setEnabled(true);
                    btnSettingsCancel.setEnabled(true);
                    btnSettingsApply.setBackgroundColor(getActivity().getResources().getColor(R.color.gold));
                    btnSettingsCancel.setBackgroundColor(getActivity().getResources().getColor(R.color.gold));
                    txtSettingsBody.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    txtSettingsHeader.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    txtSettingsTitle.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("DEFAULT",def);
                    editor.commit();
                }
            }
        });

        if (def.equals("1")){
            switchDefualt.setChecked(true);
        }
        else if (def.equals("0")){
            switchDefualt.setChecked(false);
        }

        btnSettingsCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        btnSettingsApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putInt("FONT_SIZE",fontSize);
                editor.commit();
                switch (fontSize){
                    case 0:
                        preferences.setFontStyle(FontStyle.FontStyle0);
                        break;
                    case 1:
                        preferences.setFontStyle(FontStyle.FontStyle1);
                        break;
                    case 2:
                        preferences.setFontStyle(FontStyle.FontStyle2);
                        break;
                    case 3:
                        preferences.setFontStyle(FontStyle.FontStyle3);
                        break;
                    case 4:
                        preferences.setFontStyle(FontStyle.FontStyle4);
                        break;
                    case 5:
                        preferences.setFontStyle(FontStyle.FontStyle5);
                        break;
                    case 6:
                        preferences.setFontStyle(FontStyle.FontStyle6);
                        break;
                    case 7:
                        preferences.setFontStyle(FontStyle.FontStyle7);
                        break;
                    case 8:
                        preferences.setFontStyle(FontStyle.FontStyle8);
                        break;
                    case 9:
                        preferences.setFontStyle(FontStyle.FontStyle9);
                        break;
                    case 10:
                        preferences.setFontStyle(FontStyle.FontStyle10);
                        break;
                }
                Intent intent = new Intent(getActivity(), MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
                getActivity().finish();
                Toast.makeText(getActivity(), "Custom text size applied", Toast.LENGTH_SHORT).show();
            }
        });
        return rootView;
    }
    private void changeFontSize(int size){
        switch (size){
            case 0:
                txtSettingsTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP,25);
                txtSettingsHeader.setTextSize(TypedValue.COMPLEX_UNIT_SP,15);
                txtSettingsBody.setTextSize(TypedValue.COMPLEX_UNIT_SP,10);
                break;
            case 1:
                txtSettingsTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP,26);
                txtSettingsHeader.setTextSize(TypedValue.COMPLEX_UNIT_SP,16);
                txtSettingsBody.setTextSize(TypedValue.COMPLEX_UNIT_SP,11);
                break;
            case 2:
                txtSettingsTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP,27);
                txtSettingsHeader.setTextSize(TypedValue.COMPLEX_UNIT_SP,17);
                txtSettingsBody.setTextSize(TypedValue.COMPLEX_UNIT_SP,12);
                break;
            case 3:
                txtSettingsTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP,28);
                txtSettingsHeader.setTextSize(TypedValue.COMPLEX_UNIT_SP,18);
                txtSettingsBody.setTextSize(TypedValue.COMPLEX_UNIT_SP,13);
                break;
            case 4:
                txtSettingsTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP,29);
                txtSettingsHeader.setTextSize(TypedValue.COMPLEX_UNIT_SP,19);
                txtSettingsBody.setTextSize(TypedValue.COMPLEX_UNIT_SP,14);
                break;
            case 5:
                txtSettingsTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP,30);
                txtSettingsHeader.setTextSize(TypedValue.COMPLEX_UNIT_SP,20);
                txtSettingsBody.setTextSize(TypedValue.COMPLEX_UNIT_SP,15);
                break;
            case 6:
                txtSettingsTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP,31);
                txtSettingsHeader.setTextSize(TypedValue.COMPLEX_UNIT_SP,21);
                txtSettingsBody.setTextSize(TypedValue.COMPLEX_UNIT_SP,16);
                break;
            case 7:
                txtSettingsTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP,32);
                txtSettingsHeader.setTextSize(TypedValue.COMPLEX_UNIT_SP,22);
                txtSettingsBody.setTextSize(TypedValue.COMPLEX_UNIT_SP,17);
                break;
            case 8:
                txtSettingsTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP,33);
                txtSettingsHeader.setTextSize(TypedValue.COMPLEX_UNIT_SP,23);
                txtSettingsBody.setTextSize(TypedValue.COMPLEX_UNIT_SP,18);
                break;
            case 9:
                txtSettingsTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP,34);
                txtSettingsHeader.setTextSize(TypedValue.COMPLEX_UNIT_SP,24);
                txtSettingsBody.setTextSize(TypedValue.COMPLEX_UNIT_SP,19);
                break;
            case 10:
                txtSettingsTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP,35);
                txtSettingsHeader.setTextSize(TypedValue.COMPLEX_UNIT_SP,25);
                txtSettingsBody.setTextSize(TypedValue.COMPLEX_UNIT_SP,20);
                break;
        }
    }

}
