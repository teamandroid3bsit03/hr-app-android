package com.example.daniel.HR_App;


import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by Luigi on 30/01/2018.
 */

public class HelpFragment extends Fragment {

    private ViewPager mSlideViewPager;
    private LinearLayout mDotsLayout;

    private Button mNextBtn;
    private Button mBackBtn;

    private int mCurrentPage;

    //3 Dots at the bottom of the Screen (Dots Indicator)
    private TextView[] mDots;

    //SliderAdapter Class
    private SliderAdapter sliderAdapter;


    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_help, container, false);

        mSlideViewPager = (ViewPager) rootView.findViewById(R.id.slideViewPager);
        mDotsLayout = (LinearLayout) rootView.findViewById(R.id.dotsLayout);

        mNextBtn = (Button) rootView.findViewById(R.id.btnNext);
        mBackBtn = (Button) rootView.findViewById(R.id.btnPrevious);

        sliderAdapter = new SliderAdapter(this.getActivity());
        mSlideViewPager.setAdapter(sliderAdapter);

        //Add the dots on the screen
        addDotsIndicator(0);

        //Change the color of the dot to white depending on the screen position
        mSlideViewPager.addOnPageChangeListener(viewListener);

        //OnClickListener for the Next Button
        mNextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mSlideViewPager.setCurrentItem(mCurrentPage + 1);

            }
        });

        //OnClickListener for the Back Button
        mBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mSlideViewPager.setCurrentItem(mCurrentPage - 1);

            }
        });

        return rootView;
    }

    public void addDotsIndicator(int position){

        mDots = new TextView[7];
        mDotsLayout.removeAllViews();

        for (int i = 0; i < mDots.length; i++){

            mDots[i] = new TextView(this.getActivity());
            mDots[i].setText(Html.fromHtml("&#8226"));
            mDots[i].setTextSize(35);
            mDots[i].setTextColor(getResources().getColor(R.color.colorTransparentWhite));

            mDotsLayout.addView(mDots[i]);
        }

        if (mDots.length > 0){

            mDots[position].setTextColor(getResources().getColor(R.color.white));

        }
    }

    //Listening to the changes in screen
    ViewPager.OnPageChangeListener viewListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int i, float v, int i1) {

        }

        @Override
        public void onPageSelected(int i) {

            addDotsIndicator(i);

            mCurrentPage = i;

            if (i == 0){

                mNextBtn.setEnabled(true);
                mBackBtn.setEnabled(false);
                mBackBtn.setVisibility(View.INVISIBLE);

                mNextBtn.setText("Next");
                mBackBtn.setText("");

            } else if (i == mDots.length - 1){

                mNextBtn.setEnabled(false);
                mBackBtn.setEnabled(true);
                mBackBtn.setVisibility(View.VISIBLE);
                mNextBtn.setVisibility(View.INVISIBLE);

                mNextBtn.setText("");
                mBackBtn.setText("Back");

            } else {

                mNextBtn.setEnabled(true);
                mBackBtn.setEnabled(true);
                mBackBtn.setVisibility(View.VISIBLE);
                mNextBtn.setVisibility(View.VISIBLE);

                mNextBtn.setText("Next");
                mBackBtn.setText("Back");

            }

        }

        @Override
        public void onPageScrollStateChanged(int i) {

        }
    };
}
