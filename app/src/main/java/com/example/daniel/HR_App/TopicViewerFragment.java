package com.example.daniel.HR_App;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Daniel on 06/02/2018.
 */

public class TopicViewerFragment extends Fragment {

    View rootView;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        MainActivity activity = (MainActivity)getActivity();
        activity.searchView.setVisibility(View.INVISIBLE);
        MenuItem menuItem = activity.menu1.findItem(R.id.item_add);
        menuItem.setVisible(true);

        Bundle bundle = this.getArguments();
        int group, item;
        if (bundle != null){
            group = bundle.getInt("group") ;
            item = bundle.getInt("item") ;
            switch (group){
                case 0:
                    switch (item){
                        case 0:
                            rootView = inflater.inflate(R.layout.t_a_01, container, false);
                            break;
                        case 1:
                            rootView = inflater.inflate(R.layout.t_a_02, container, false);
                            break;
                        case 2:
                            rootView = inflater.inflate(R.layout.t_a_03, container, false);
                            break;
                        case 3:
                            rootView = inflater.inflate(R.layout.t_a_04, container, false);
                            break;
                        case 4:
                            rootView = inflater.inflate(R.layout.t_a_05, container, false);
                            break;
                        case 5:
                            rootView = inflater.inflate(R.layout.t_a_06, container, false);
                            break;
                        default:
                            break;
                    }
                    break;
                case 1:
                    switch (item){
                        case 0:
                            rootView = inflater.inflate(R.layout.t_c_07, container, false);
                            break;
                        case 1:
                            rootView = inflater.inflate(R.layout.t_c_08, container, false);
                            break;
                        case 2:
                            rootView = inflater.inflate(R.layout.t_c_09, container, false);
                            break;
                        case 3:
                            rootView = inflater.inflate(R.layout.t_c_10, container, false);
                            break;
                        case 4:
                            rootView = inflater.inflate(R.layout.t_c_11, container, false);
                            break;
                        case 5:
                            rootView = inflater.inflate(R.layout.t_c_12, container, false);
                            break;
                        case 6:
                            rootView = inflater.inflate(R.layout.t_c_13, container, false);
                            break;
                        case 7:
                            rootView = inflater.inflate(R.layout.t_c_14, container, false);
                            break;
                        default:
                            break;
                    }
                    break;
                case 2:
                    switch (item){
                        case 0:
                            rootView = inflater.inflate(R.layout.t_p_15, container, false);
                            break;
                        case 1:
                            rootView = inflater.inflate(R.layout.t_p_16, container, false);
                            break;
                        case 2:
                            rootView = inflater.inflate(R.layout.t_p_17, container, false);
                            break;
                        case 3:
                            rootView = inflater.inflate(R.layout.t_p_18, container, false);
                            break;
                        case 4:
                            rootView = inflater.inflate(R.layout.t_p_19, container, false);
                            break;
                        default:
                            break;
                    }
                    break;
                case 3:
                    switch (item){
                        case 0:
                            rootView = inflater.inflate(R.layout.t_s_20, container, false);
                            break;
                        case 1:
                            rootView = inflater.inflate(R.layout.t_s_21, container, false);
                            break;
                        case 2:
                            rootView = inflater.inflate(R.layout.t_s_22, container, false);
                            break;
                        case 3:
                            rootView = inflater.inflate(R.layout.t_s_23, container, false);
                            break;
                        case 4:
                            rootView = inflater.inflate(R.layout.t_s_24, container, false);
                            break;
                        case 5:
                            rootView = inflater.inflate(R.layout.t_s_25, container, false);
                            break;
                        case 6:
                            rootView = inflater.inflate(R.layout.t_s_26, container, false);
                            break;
                        case 7:
                            rootView = inflater.inflate(R.layout.t_s_27, container, false);
                            break;
                        case 8:
                            rootView = inflater.inflate(R.layout.t_s_28, container, false);
                            break;
                        case 9:
                            rootView = inflater.inflate(R.layout.t_s_29, container, false);
                            break;
                        case 10:
                            rootView = inflater.inflate(R.layout.t_s_30, container, false);
                            break;
                        case 11:
                            rootView = inflater.inflate(R.layout.t_s_31, container, false);
                            break;
                        case 12:
                            rootView = inflater.inflate(R.layout.t_s_32, container, false);
                            break;
                        case 13:
                            rootView = inflater.inflate(R.layout.t_s_33, container, false);
                            break;
                        case 14:
                            rootView = inflater.inflate(R.layout.t_s_34, container, false);
                            break;
                        case 15:
                            rootView = inflater.inflate(R.layout.t_s_35, container, false);
                            break;
                        case 16:
                            rootView = inflater.inflate(R.layout.t_s_36, container, false);
                            break;
                        case 17:
                            rootView = inflater.inflate(R.layout.t_s_37, container, false);
                            break;
                        case 18:
                            rootView = inflater.inflate(R.layout.t_s_38, container, false);
                            break;
                        case 19:
                            rootView = inflater.inflate(R.layout.t_s_39, container, false);
                            break;
                        case 20:
                            rootView = inflater.inflate(R.layout.t_s_40, container, false);
                            break;
                        case 21:
                            rootView = inflater.inflate(R.layout.t_s_41, container, false);
                            break;
                        case 22:
                            rootView = inflater.inflate(R.layout.t_s_42, container, false);
                            break;
                        case 23:
                            rootView = inflater.inflate(R.layout.t_s_43, container, false);
                            break;
                        case 24:
                            rootView = inflater.inflate(R.layout.t_s_44, container, false);
                            break;
                        case 25:
                            rootView = inflater.inflate(R.layout.t_s_45, container, false);
                            break;
                        case 26:
                            rootView = inflater.inflate(R.layout.t_s_46, container, false);
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }
        }
        return rootView;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.item_add){
            Intent intents = new Intent(getContext(), AddNoteActivity.class);
            intents.putExtra("switch", "1");
            startActivity(intents);
        }
        return super.onOptionsItemSelected(item);
    }
}
