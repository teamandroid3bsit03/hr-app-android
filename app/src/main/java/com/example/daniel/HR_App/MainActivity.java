package com.example.daniel.HR_App;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    ScrollView homelayout;
    Button btnMore,btnGotoExpandedList;
    ActionBarDrawerToggle toggle;
    DrawerLayout drawerLayout;
    Fragment fragment, curFragment, prevFragment;
    public Menu menu1;
    SearchView searchView;
    Bundle bundle;
    public String search;
    public List<Integer> parentGroup;
    LinearLayout linearContainer, segmentContainer;
    NavigationView navigationView;
    FrameLayout frameLayout;
    public Boolean searchTopics, searcherOpen,showAllValuesTopics,showAllValuesTerms;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getTheme().applyStyle(new Preferences(this).getFontStyle().getResId(), true);
        setContentView(R.layout.activity_main);

        frameLayout = (FrameLayout)findViewById(R.id.frameLayout);
        homelayout = (ScrollView)findViewById(R.id.HomeLayout);
        btnMore = (Button)findViewById(R.id.btnMore);
        btnGotoExpandedList = (Button) findViewById(R.id.btnGotoExpandedList);
        searchView = (SearchView)findViewById(R.id.searchView);
        linearContainer = (LinearLayout)findViewById(R.id.linearContainer);
        segmentContainer = (LinearLayout)findViewById(R.id.segmentContainer);

        toolbar = (Toolbar)findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);

        drawerLayout = (DrawerLayout)findViewById(R.id.drawerLayout);
        toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.open,R.string.close);
        drawerLayout.setDrawerListener(toggle);
        toggle.syncState();

        fragment = null;
        curFragment = null;
        search = "";
        searchTopics = true;
        showAllValuesTerms = false;
        showAllValuesTopics = false;
        searcherOpen = false;
        parentGroup = new ArrayList<>();

        btnMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment = new AboutFragment();
                navigationView.setCheckedItem(R.id.menuAbout);
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction().addToBackStack("tag");
                transaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right,android.R.anim.slide_in_left,android.R.anim.slide_out_right );
                transaction.replace(R.id.frameLayout, fragment).commit();
                curFragment = fragment;
                searchView.setVisibility(View.INVISIBLE);
            }
        });
        btnGotoExpandedList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment = new ExpTopicListFragment();
                navigationView.setCheckedItem(R.id.menuTopics);
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction().addToBackStack("tag");
                transaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right,android.R.anim.slide_in_left,android.R.anim.slide_out_right );
                transaction.replace(R.id.frameLayout, fragment).commit();
                curFragment = fragment;
            }
        });

        //replace navigation drawer with back function
        searchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search = "";
                if (fragment == null){
                    commitFragmentWithAnimation(new SearcherFragmet());
                }
                else if (fragment.getClass() == ExpTopicListFragment.class ){
                    Log.i("Fragment", "" + fragment.getClass().toString());
                    fragment = new TopicListFragment();
                    commitFragmentWithAnimation(new TopicListFragment());
                    showAllValuesTopics = false;
                }
                else if (fragment.getClass() == TermsListFragement.class){
                    Log.i("Fragment", "" + fragment.getClass().toString());
                    fragment = new TermsListFragement();
                    commitFragmentWithAnimation(new TermsListFragement());
                    showAllValuesTerms = false;
                    searcherOpen = true;
                }
                changeDrawerToBackButton();
            }
        });
        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                if (fragment == null ||fragment.getClass() == SearcherFragmet.class ){
                    clearFragmentGoToHomePage();
                    return true;
                }
                else if (fragment.getClass() == TopicViewerFragment.class || fragment.getClass() == TermViewerFragment.class){
                    onBackPressed();
                }
                else if (fragment.getClass() == TopicListFragment.class ){
                    commitFragmentWithAnimation(new ExpTopicListFragment());
                    showAllValuesTopics = false;
                }
                else if (fragment.getClass() == TermsListFragement.class){
                    commitFragmentWithAnimation(new TermsListFragement());
                    showAllValuesTerms = false;
                    returnToDefaultDrawer();
                }

                return false;
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                search = s;
                if (fragment.getClass() == SearcherFragmet.class){
                    commitFragmentWithOutAnimation(new SearcherFragmet());
                }
                else if (fragment.getClass() == TopicListFragment.class){
                    commitFragmentWithOutAnimation(new TopicListFragment());
                }
                else if (fragment.getClass() == TermsListFragement.class){
                    commitFragmentWithOutAnimation(new TermsListFragement());
                }
                return true;
            }
            @Override
            public boolean onQueryTextChange(String s) {
                search = s;
                if (fragment != null){
                    if (fragment.getClass() == SearcherFragmet.class){
                        commitFragmentWithOutAnimation(new SearcherFragmet());
                    }
                    else if (fragment.getClass() == TopicListFragment.class){
                        commitFragmentWithOutAnimation(new TopicListFragment());
                    }
                    else if (fragment.getClass() == TermsListFragement.class){
                        commitFragmentWithOutAnimation(new TermsListFragement());
                    }
                }
                return true;
            }
        });

        navigationView = (NavigationView) findViewById(R.id.navigationView);
        navigationView.setCheckedItem(R.id.menuHome);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                fragment = null;
                if (id == R.id.menuHome){
                    if (!searchView.isIconified()){
                        setSearchViewToIcon();
                    }
                    curFragment = null;
                    fragment = null;
                    searchView.setVisibility(View.VISIBLE);
                    invalidateOptionsMenu();
                    drawerLayout.closeDrawer(Gravity.LEFT);
                    clearBackStack();
                    frameLayout.removeAllViews();
                    frameLayout.addView(homelayout);
                    navigationView.setCheckedItem(R.id.menuHome);
                }
                else if (id == R.id.menuTopics){
                    fragment = new ExpTopicListFragment();
                }
                else if (id == R.id.menuTerms){
                    showAllValuesTerms  = true;
                    fragment = new TermsListFragement();
                }
                else if (id == R.id.menuNotes){
                    fragment = new NotesFragment();
                }
                else if (id == R.id.menuSettings){
                    fragment = new SettingsFragment();
                }
                else if (id == R.id.menuHelp){
                    fragment = new HelpFragment();
                }
                else if (id == R.id.menuAbout){
                    fragment = new AboutFragment();

                }
                if (fragment != null){
                    prevFragment = fragment;
                    if (fragment.getClass() == NotesFragment.class || fragment.getClass() == HelpFragment.class || fragment.getClass() == SettingsFragment.class || fragment.getClass() == AboutFragment.class ){
                        if (fragment.getClass() == NotesFragment.class ){
                            MenuItem menuItem = menu1.findItem(R.id.item_add);
                            menuItem.setVisible(true);
                        }
                        else {
                            invalidateOptionsMenu();
                        }
                        searchView.setVisibility(View.INVISIBLE);
                    }
                    else {
                        searchView.setVisibility(View.VISIBLE);
                        invalidateOptionsMenu();
                    }
                    if (curFragment != null){
                        if (fragment.getClass() == curFragment.getClass()){
                            drawerLayout.closeDrawer(Gravity.LEFT);
                            return true;
                        }
                        else{
                            drawerLayout.closeDrawer(GravityCompat.START);
                            frameLayout.removeAllViews();

                            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction().addToBackStack("tag");
                            transaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right,android.R.anim.slide_in_left,android.R.anim.slide_out_right );
                            transaction.replace(R.id.frameLayout, fragment).commit();
                            curFragment = fragment;
                            return true;
                        }
                    }
                    else {
                        drawerLayout.closeDrawer(GravityCompat.START);
                        frameLayout.removeAllViews();

                        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction().addToBackStack("tag");
                        transaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right,android.R.anim.slide_in_left,android.R.anim.slide_out_right );
                        transaction.replace(R.id.frameLayout, fragment).commit();
                        curFragment = fragment;
                        return true;
                    }


                }
                return false;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == R.id.item_add){
            Intent intent = new Intent(getApplicationContext(), AddNoteActivity.class);
            intent.putExtra("switch", "1");
            startActivity(intent);
        }
        if (toggle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_menu, menu);
        menu1 = menu;
        return true;
    }

    @Override
    public void onBackPressed() {
        invalidateOptionsMenu();
        searchView.setVisibility(View.VISIBLE);
        if (drawerLayout.isDrawerOpen(GravityCompat.START)){
            drawerLayout.closeDrawer(GravityCompat.START);
        }
        else{
            if (fragment != null){
                if (fragment.getClass() == TermViewerFragment.class){
                    commitFragmentWithAnimation(new TermsListFragement());
                    returnToDefaultDrawer();
                    searchView.setVisibility(View.VISIBLE);
                }
                else if (fragment.getClass() == TopicViewerFragment.class){
                    if (prevFragment.getClass() == ExpTopicListFragment.class){
                        returnToDefaultDrawer();
                        commitFragmentWithAnimation(new ExpTopicListFragment());
                        invalidateOptionsMenu();
                        searchView.setVisibility(View.VISIBLE);
                    }
                    else {
                        if (prevFragment.getClass() == TermsListFragement.class && !searcherOpen){
                            returnToDefaultDrawer();
                        }
                        fragment = prevFragment;
                        commitFragmentWithAnimation(fragment);
                        invalidateOptionsMenu();
                        searchView.setVisibility(View.VISIBLE);
                        prevFragment = null;
                        searcherOpen = false;
                    }
                }
                else if (fragment.getClass() == SearcherFragmet.class){
                    Log.i("BackPressed", "onBackPressed: Searcher Fragment Returns to home");
                    clearFragmentGoToHomePage();
                    prevFragment = null;
                }
                else if (fragment.getClass() == TopicListFragment.class){
                    if (!showAllValuesTopics){
                        setSearchViewToIcon();
                        returnToDefaultDrawer();
                        commitFragmentWithAnimation(new ExpTopicListFragment());
                        searchView.setVisibility(View.VISIBLE);
                    }
                }
                else if ( fragment.getClass() == TermsListFragement.class){
                    if (!showAllValuesTerms){
                        setSearchViewToIcon();
                        showAllValuesTerms = true;
                        commitFragmentWithAnimation(new TermsListFragement());
                        returnToDefaultDrawer();
                        searchView.setVisibility(View.VISIBLE);
                    }
                    else {
                        clearBackStack();
                        clearFragmentGoToHomePage();
                        navigationView.setCheckedItem(R.id.menuHome);
                    }
                }
                else{
                    Log.i("BackPressed", "onBackPressed: Everything else Returns to home");
                    clearBackStack();
                    clearFragmentGoToHomePage();
                    navigationView.setCheckedItem(R.id.menuHome);
                }
            }
            else {
                Log.i("BackPressed", "onBackPressed: Exit App");
                super.onBackPressed();
            }
        }
    }
    //Change the logo of nav drawer and make it function like back button
    public void changeDrawerToBackButton(){
        toolbar.setNavigationIcon(R.drawable.ic_action_home_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (fragment.getClass() == SearcherFragmet.class ){
                    Log.i("TooolbarBackPressed", "onBackPressed: From search to home");
                    clearFragmentGoToHomePage();
                }
                else if(fragment.getClass() == TopicViewerFragment.class ){
                    if (prevFragment.getClass() == ExpTopicListFragment.class){
                        returnToDefaultDrawer();
                        commitFragmentWithAnimation(new ExpTopicListFragment());
                        invalidateOptionsMenu();
                        searchView.setVisibility(View.VISIBLE);
                    }
                    else {
                        if (prevFragment.getClass() == TermsListFragement.class && !searcherOpen){
                            returnToDefaultDrawer();
                        }
                        fragment = prevFragment;
                        commitFragmentWithAnimation(fragment);
                        invalidateOptionsMenu();
                        searchView.setVisibility(View.VISIBLE);
                        prevFragment = null;
                        searcherOpen = false;
                    }
                }
                else if (fragment.getClass() == TopicListFragment.class ){
                    Log.i("TooolbarBackPressed", "onBackPressed: Return to default drawer");
                    commitFragmentWithAnimation(new ExpTopicListFragment());
                    returnToDefaultDrawer();
                    searchView.setVisibility(View.VISIBLE);
                }
                else if (fragment.getClass() == TermsListFragement.class ){
                    Log.i("TooolbarBackPressed", "onBackPressed: Return to default drawer");
                    commitFragmentWithAnimation(new TermsListFragement());
                    returnToDefaultDrawer();
                    searchView.setVisibility(View.VISIBLE);
                    showAllValuesTerms = true;
                }
                else if (fragment.getClass() == TermViewerFragment.class){
                    commitFragmentWithAnimation(new TermsListFragement());
                    returnToDefaultDrawer();
                    searchView.setVisibility(View.VISIBLE);
                    showAllValuesTerms = true;
                    searcherOpen = false;
                }
            }
        });
    }
    public void clearFragmentGoToHomePage(){
        search = "";

        prevFragment = null ;
        curFragment = null;
        fragment = null;
        searchView.setVisibility(View.VISIBLE);
        invalidateOptionsMenu();

        returnToDefaultDrawer();

        frameLayout.removeAllViews();
        frameLayout.addView(homelayout);
    }
    //change views with animation
    public void commitFragmentWithAnimation(Fragment activity){
        this.fragment = activity ;
        FragmentTransaction frm = getSupportFragmentManager().beginTransaction();
        frm.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right,android.R.anim.slide_in_left,android.R.anim.slide_out_right );
        frm.replace(R.id.frameLayout, fragment).commit();
    }
    public void commitFragmentWithOutAnimation(Fragment activity){
        this.fragment = activity ;
        FragmentTransaction frm1 = getSupportFragmentManager().beginTransaction();
        frm1.setCustomAnimations(0,0,android.R.anim.slide_in_left,android.R.anim.slide_out_right );
        frm1.replace(R.id.frameLayout, fragment).commit();
    }
    //close search view and return the drawer icon and function to defualt.
    public void returnToDefaultDrawer(){
        searchView.onActionViewCollapsed();
        drawerLayout = (DrawerLayout)findViewById(R.id.drawerLayout);
        Activity activity = new MainActivity();
        toggle = new ActionBarDrawerToggle(activity, drawerLayout, toolbar, R.string.open,R.string.close);
        drawerLayout.setDrawerListener(toggle);
        toggle.syncState();
    }
    //clear back stack of fragment manager
    public void clearBackStack(){
        FragmentManager fm = getSupportFragmentManager();
        int count = fm.getBackStackEntryCount();
        for (int i = 0; i< count; ++i){
            fm.popBackStackImmediate();
        }
    }
    public void setSearchViewToIcon(){
        searchView.setIconified(true);
        linearContainer.setGravity(Gravity.RIGHT);
        searchView.getLayoutParams().width = ViewGroup.LayoutParams.WRAP_CONTENT;
    }

}
