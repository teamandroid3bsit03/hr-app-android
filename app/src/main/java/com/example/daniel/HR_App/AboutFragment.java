package com.example.daniel.HR_App;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by Daniel on 30/01/2018.
 */

public class AboutFragment extends Fragment{

    TextView tvUpangLink;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_about, container, false);

        tvUpangLink = (TextView) rootView.findViewById(R.id.tvUpangLink);
        tvUpangLink.setMovementMethod(LinkMovementMethod.getInstance());

        tvUpangLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.phinma.edu.ph/"));
                startActivity(intent);
            }
        });
        return rootView;
    }
}
