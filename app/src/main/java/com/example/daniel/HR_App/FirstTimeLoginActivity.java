package com.example.daniel.HR_App;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class FirstTimeLoginActivity extends AppCompatActivity {
    EditText etxtLogin;
    Button btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_time_login);


        etxtLogin = (EditText)findViewById(R.id.etxtLogin);
        btnLogin = (Button)findViewById(R.id.btnNext);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String account = etxtLogin.getText().toString();
                String[] separated = account.split("@");
                if (separated.length == 2){
                    if (separated[1].equals("up.phinma.edu.ph")){
                        new SendEmailAysc().execute(account,"BAKASYON NA  >> phinma << yung code");
                        Intent intent = new Intent(getApplicationContext(), CodeVerification.class);
                        startActivity(intent);
                        finish();
                    }
                    else {
                        etxtLogin.setError("Invalid email!");
                    }
                }
                else {
                    etxtLogin.setError("Invalid email!");
                }
            }
        });
    }
}
