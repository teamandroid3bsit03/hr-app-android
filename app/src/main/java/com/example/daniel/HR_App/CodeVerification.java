package com.example.daniel.HR_App;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class CodeVerification extends AppCompatActivity {

    EditText etxtCode;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_code_verification);

        Button btnVerify = (Button)findViewById(R.id.btnVerify);
        etxtCode = (EditText)findViewById(R.id.etxtCode);

        btnVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String code = etxtCode.getText().toString();
                if (code.equals("phinma")){
                    Intent intent = new Intent(getApplicationContext(), TermsAndAgreementActivity.class);
                    startActivity(intent);
                    finish();
                }
                else {
                    etxtCode.setError("Invalid verification code!");
                }
            }
        });
    }
}
