package com.example.daniel.HR_App;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

public class SplashScreen extends AppCompatActivity {

    ImageView ivSplashLogo;
    TextView tvHeading;
    TextView tvSubHeading;
    Animation animation;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        // A way to hide the status bar but i did not use it, instead i used the windowTranslucentStatus on the styles.xml
        //    that is supported on version 19 up.
        /*//Hides the Status Bar
        View decorView = getWindow().getDecorView();
        // Hide the status bar.
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);*/

        ivSplashLogo = (ImageView) findViewById(R.id.phinmaLogoIV);
        tvHeading = (TextView) findViewById(R.id.phinmaHeaderTV);
        tvSubHeading = (TextView) findViewById(R.id.phinmaSubHeaderTV);
        animation = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fade);
        ivSplashLogo.startAnimation(animation);
        tvHeading.startAnimation(animation);
        tvSubHeading.startAnimation(animation);

        Thread timer = new Thread(){

            @Override
            public void run() {

                try {

                    sleep(3000);

                    sharedPreferences = getApplicationContext().getSharedPreferences("SETTINGS", Context.MODE_PRIVATE);
                    String firstTime = sharedPreferences.getString("FIRST_TIME","1");
                    if (firstTime.equals("1")){
                        Intent intent = new Intent(getApplicationContext(),FirstTimeLoginActivity.class);
                        startActivity(intent);
                        finish();
                    }
                    else {
                        Intent intent = new Intent(getApplicationContext(),MainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                    super.run();

                } catch (InterruptedException e) {

                    e.printStackTrace();

                }
            }
        };
        timer.start();
    }
}
