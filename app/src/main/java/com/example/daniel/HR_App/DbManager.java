package com.example.daniel.HR_App;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

/**
 * Created by Daniel on 20/01/2018.
 */

public class DbManager {
    Context context;
    DbHelper dbHelper;
    SQLiteDatabase sqLiteDatabase;

    public DbManager(Context context) {
        this.context = context;
    }
     public DbManager open() throws SQLiteException{
         dbHelper = new DbHelper(context);
         sqLiteDatabase = dbHelper.getReadableDatabase();
         return this;
     }

     public void close(){
         dbHelper.close();
     }

    public void insert(String title, String date, String content){
        ContentValues contentValues = new ContentValues();
        contentValues.put(dbHelper.TITLE, title);
        contentValues.put(dbHelper.DATE, date);
        contentValues.put(dbHelper.CONTENT, content);
        sqLiteDatabase.insert("notes", null, contentValues);
    }

    public Cursor fetch(String table,String columns[]){
        Cursor cursor = sqLiteDatabase.query(table, columns, null, null, null, null, null);
        if (cursor != null){
            cursor.moveToFirst();
        }
        return cursor;
    }
    public Cursor fetchNote(String table,String columns[]){
        Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM " + table + " ORDER BY DATETIME(date) ASC", null);
        if (cursor != null){
            cursor.moveToFirst();
        }
        return cursor;
    }

    public Cursor filteredFetchTerms(CharSequence charSequence){
        Cursor cursor = sqLiteDatabase.rawQuery("SELECT keywordtopics._id ,keyword FROM keywordtopics WHERE keyword like '" + charSequence.toString() + "%' GROUP BY keyword;", null);
        if (cursor != null){
            cursor.moveToFirst();
        }
        return cursor;
    }

    public Cursor filteredFetchTopics(CharSequence charSequence){
        Cursor cursor = sqLiteDatabase.rawQuery("SELECT DISTINCT topics._id, topic, groupno, itemno FROM topics INNER JOIN keywordtopics on keywordtopics.topicid = topics._id WHERE topics.topic like '" + charSequence.toString() + "%' OR keywordtopics.keyword like '" + charSequence.toString() +"%';", null);
        if (cursor != null){
            cursor.moveToFirst();
        }
        return cursor;
    }

    public Cursor fetchTopicForSelectedTag(CharSequence charSequence){
        Cursor cursor = sqLiteDatabase.rawQuery("SELECT topics._id, topic, groupno, itemno FROM topics INNER JOIN keywordtopics on keywordtopics.topicid = topics._id WHERE keywordtopics.keyword like '" + charSequence.toString() +"%';", null);
        if (cursor != null){
            cursor.moveToFirst();
        }
        return cursor;
    }
    public Cursor fetchTags(){
        Cursor cursor = sqLiteDatabase.rawQuery("SELECT keywordtopics._id,  keyword FROM keywordtopics GROUP BY keyword ;", null);
        if (cursor != null){
            cursor.moveToFirst();
        }
        return cursor;
    }


    public int update(int id,String title, String date, String content){
        ContentValues contentValues = new ContentValues();
        contentValues.put(dbHelper.TITLE, title);
        contentValues.put(dbHelper.DATE, date);
        contentValues.put(dbHelper.CONTENT, content);
        int x = sqLiteDatabase.update("notes", contentValues, "_id = "+ id, null);
        return x;
    }

    public void delete(String id){
        sqLiteDatabase.delete("notes", "_id =" + id, null);
    }
}
