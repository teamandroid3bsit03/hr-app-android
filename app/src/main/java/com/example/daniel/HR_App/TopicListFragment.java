package com.example.daniel.HR_App;

import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Daniel on 30/01/2018.
 */

public class TopicListFragment extends Fragment {

    ListView listViewTopics;
    DbManager dbManager;
    Cursor cursor;
    SimpleCursorAdapter adapter;
    String from [] = {"_id","topic","groupno","itemno"};
    int to [] = {R.id.txtIdTerms ,R.id.txtWordTerms,R.id.txtGno,R.id.txtItemno};
    String searched;
    LinearLayout topicListContainer;
    MainActivity activity;
    TextView txtTopicListQueryResult;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_list_topics, container, false);

        listViewTopics = (ListView)rootView.findViewById(R.id.listViewTopics);
        topicListContainer = (LinearLayout) rootView.findViewById(R.id.topicListContainer);
        txtTopicListQueryResult = rootView.findViewById(R.id.txtTopicListQueryResult);

        dbManager = new DbManager(getActivity());
        dbManager.open();

        activity = (MainActivity)getActivity();
        searched = activity.search;

        if (searched.equals("")){
            cursor = dbManager.fetch("topics",from);
            checkResults(cursor);
        }
        else {
            cursor = dbManager.filteredFetchTopics(searched);
            checkResults(cursor);
        }

        adapter = new SimpleCursorAdapter(getActivity(), R.layout.list_terms, cursor, from, to, 0);
        adapter.notifyDataSetChanged();
        listViewTopics.setAdapter(adapter);
        listViewTopics.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                TextView t1 = view.findViewById(R.id.txtWordTerms);
                TextView t2 = view.findViewById(R.id.txtIdTerms);
                TextView t3 = view.findViewById(R.id.txtGno);
                TextView t4 = view.findViewById(R.id.txtItemno);

                String topic = t1.getText().toString();
                String id = t2.getText().toString();
                int group = Integer.parseInt(t3.getText().toString());
                int item = Integer.parseInt(t4.getText().toString());

                Bundle bundlex = new Bundle();
                bundlex.putString("term",topic);
                bundlex.putString("id",id);
                bundlex.putInt("group",group);
                bundlex.putInt("item",item);
                bundlex.putString("search","no");

                Fragment fragment1 = new TopicViewerFragment();

                activity.fragment = fragment1;
                activity.prevFragment = new TopicListFragment();
                activity.changeDrawerToBackButton();

                fragment1.setArguments(bundlex);
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right,android.R.anim.slide_in_left,android.R.anim.slide_out_right );
                transaction.replace(R.id.frameLayout, fragment1).commit();
            }
        });
        return rootView;
    }
    public void checkResults(Cursor cursor){
        if (searched.equals("") && !activity.showAllValuesTopics){
            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)topicListContainer.getLayoutParams();
            params.height = 0;
            topicListContainer.setLayoutParams(params);
            txtTopicListQueryResult.setText("Start typing to search");
        }
        else {
            if (cursor.getCount() == 0){
                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)topicListContainer.getLayoutParams();
                params.height = 0;
                topicListContainer.setLayoutParams(params);
                txtTopicListQueryResult.setText("No match found");
            }
            else {
                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)topicListContainer.getLayoutParams();
                params.height = ViewGroup.LayoutParams.MATCH_PARENT;
                topicListContainer.setLayoutParams(params);
            }
        }
    }
}
