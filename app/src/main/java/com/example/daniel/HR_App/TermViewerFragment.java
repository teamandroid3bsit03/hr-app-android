package com.example.daniel.HR_App;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Daniel on 30/01/2018.
 */

public class TermViewerFragment extends Fragment{

    ListView listViewTerms;
    DbManager dbManager;
    Cursor cursor;
    SimpleCursorAdapter adapter;
    String from [] = {"_id","topic","groupno","itemno"};
    int to [] = {R.id.txtIdTerms ,R.id.txtWordTerms,R.id.txtGno,R.id.txtItemno};
    String tagSelected;
    MainActivity activity;
    Boolean fromSearcher;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_list_topics, container, false);

        listViewTerms = (ListView)rootView.findViewById(R.id.listViewTopics);

        activity = (MainActivity)getActivity();
        activity.searchView.setVisibility(rootView.INVISIBLE);

        dbManager = new DbManager(getActivity());
        dbManager.open();

        Bundle bundle = this.getArguments();
        if (bundle != null){
            tagSelected =  bundle.getString("term");
            fromSearcher = bundle.getBoolean("fromSearcher");
            cursor = dbManager.fetchTopicForSelectedTag(tagSelected);
        }

        adapter = new SimpleCursorAdapter(getActivity(), R.layout.list_terms, cursor, from, to, 0);
        adapter.notifyDataSetChanged();
        listViewTerms.setAdapter(adapter);
        listViewTerms.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                TextView t1 = view.findViewById(R.id.txtWordTerms);
                TextView t2 = view.findViewById(R.id.txtIdTerms);
                TextView t3 = view.findViewById(R.id.txtGno);
                TextView t4 = view.findViewById(R.id.txtItemno);

                String topic = t1.getText().toString();
                String id = t2.getText().toString();
                int group = Integer.parseInt(t3.getText().toString());
                int item = Integer.parseInt(t4.getText().toString());

                Bundle bundlex = new Bundle();
                bundlex.putString("term",topic);
                bundlex.putString("id",id);
                bundlex.putInt("group",group);
                bundlex.putInt("item",item);
                bundlex.putString("search","no");

                Fragment fragment1 = new TopicViewerFragment();

                activity.changeDrawerToBackButton();
                activity.fragment = fragment1;
                if (fromSearcher){
                    activity.prevFragment = new SearcherFragmet();
                }
                else {
                    activity.prevFragment = new TermsListFragement();
                }
                fragment1.setArguments(bundlex);
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right,android.R.anim.slide_in_left,android.R.anim.slide_out_right );
                transaction.replace(R.id.frameLayout, fragment1).commit();

            }
        });

        return rootView;
    }
}
