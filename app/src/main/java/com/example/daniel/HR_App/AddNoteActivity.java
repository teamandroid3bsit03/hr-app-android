package com.example.daniel.HR_App;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class AddNoteActivity extends Activity {

    EditText etxtTitle, etxtContent;
    String tits, conts;
    Button btnDone, btnCancel;
    int id;
    DbManager dbManager;
    boolean edit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pop_up_add_note);

        etxtTitle = (EditText)findViewById(R.id.etxtName);
        etxtContent = (EditText)findViewById(R.id.etxtContent);

        btnDone = (Button)findViewById(R.id.btnDone);
        btnCancel = (Button)findViewById(R.id.btnCancel);

        dbManager = new DbManager(getApplicationContext());

        Intent intent = getIntent();
        if (intent.getStringExtra("switch").toString().equals("1")){
            btnDone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(etxtTitle.getText().toString().equals("") && etxtContent.getText().toString().equals("")){
                        tastyToast("Note Deleted");
                        finish();
                    }
                    else if (etxtTitle.getText().toString().equals("") || etxtContent.getText().toString().equals("")){
                        tastyToast("Fill in all the fields");
                    }
                    else{
                        dbManager.open();
                        String dateTime = new SimpleDateFormat("H:mm a MMM. dd, yyyy", Locale.getDefault()).format(new Date());
                        dbManager.insert(etxtTitle.getText().toString().trim(), dateTime, etxtContent.getText().toString().trim());
                        tastyToast("Note Created");
                        dbManager.close();
                        finish();
                    }
                }
            });
        }
        else if (intent.getStringExtra("switch").toString().equals("0")){
            etxtTitle.setText("" + intent.getStringExtra("title"));
            etxtContent.setText(intent.getStringExtra("content"));

            id = Integer.parseInt(intent.getStringExtra("id"));
            edit = true;
            btnDone.setText("Edit");
            etxtContent.setEnabled(false);
            etxtTitle.setEnabled(false);
            btnDone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (edit){
                        etxtContent.setEnabled(true);
                        etxtTitle.setEnabled(true);
                        btnDone.setText("Done");
                        edit = false;
                    }
                    else{
                        if(etxtTitle.getText().toString().equals("") && etxtContent.getText().toString().equals("")){
                            tastyToast("Note Deleted");
                            btnDone.setText("Edit");
                            etxtContent.setEnabled(false);
                            etxtTitle.setEnabled(false);
                            etxtTitle.setFocusable(true);
                            edit = true;
                            finish();
                        }
                        else if (etxtTitle.getText().toString().equals("") || etxtContent.getText().toString().equals("")){
                            tastyToast("Fill in all the fields");
                        }
                        else{
                            dbManager.open();
                            String dateTime = new SimpleDateFormat("H:mm a MMM. dd, yyyy", Locale.getDefault()).format(new Date());
                            dbManager.update(id, etxtTitle.getText().toString().trim(), dateTime, etxtContent.getText().toString().trim());
                            tastyToast("Note Updated");
                            dbManager.close();
                            btnDone.setText("Edit");
                            etxtContent.setEnabled(false);
                            etxtTitle.setEnabled(false);
                            etxtTitle.setFocusable(true);
                            edit = true;
                            finish();

                        }

                        btnDone.setText("Edit");
                        etxtContent.setEnabled(false);
                        etxtTitle.setEnabled(false);
                        etxtTitle.setFocusable(true);
                        edit = true;


                    }

                }
            });
        }
        else if (intent.getStringExtra("switch").toString().equals("2")){
            etxtTitle.setText("" + intent.getStringExtra("title"));
            etxtContent.setText("" + intent.getStringExtra("content"));
            id = Integer.parseInt(intent.getStringExtra("id"));
            btnDone.setText("Edit");
        }


        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
    public void tastyToast(String message){
        final Toast toast = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT);
        toast.show();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                toast.cancel();
            }
        }, 700);
    }
}
