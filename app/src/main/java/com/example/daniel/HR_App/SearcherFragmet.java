package com.example.daniel.HR_App;

import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SimpleCursorAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Daniel on 30/01/2018.
 */

public class SearcherFragmet extends Fragment {

    ListView listViewSearcher;
    DbManager dbManager;
    Cursor cursor;
    SimpleCursorAdapter adapter;
    String fromTerm [] = {"_id","keyword"};
    String fromTopic[] = {"_id","topic"};
    int to [] = {R.id.txtIdTerms ,R.id.txtWordTerms};
    String searched;
    Button btnTopics, btnTerms;
    Boolean searchTopic;
    LinearLayout searchListContainer;
    MainActivity activity;
    TextView txtSearcherQuery;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_searcher, container, false);

        activity = (MainActivity)getActivity();
        searched = activity.search;
        searchTopic = activity.searchTopics;

        listViewSearcher = (ListView)rootView.findViewById(R.id.listViewSearcher);
        btnTopics = (Button)rootView.findViewById(R.id.btnTopics);
        btnTerms = (Button)rootView.findViewById(R.id.btnTerms);
        searchListContainer = (LinearLayout)rootView.findViewById(R.id.searchListContainer);
        txtSearcherQuery = rootView.findViewById(R.id.txtSearcherQuery);

        dbManager = new DbManager(getActivity());
        dbManager.open();

        if (searchTopic){
            activity.searchTopics = true;
            searchTopic = true;
            btnTopics.setBackgroundResource(R.color.colorPrimary);
            btnTopics.setTextColor(Color.parseColor("#ffffff"));
            btnTerms.setBackgroundResource(R.color.gray);
            btnTerms.setTextColor(Color.parseColor("#000000"));

            cursor = dbManager.filteredFetchTopics(searched);
            checkResults(cursor);
            adapter = new SimpleCursorAdapter(getActivity(), R.layout.list_terms, cursor, fromTopic, to, 0);
        }
        else {
            activity.searchTopics = false;
            searchTopic = false;
            btnTopics.setBackgroundResource(R.color.gray);
            btnTopics.setTextColor(Color.parseColor("#000000"));
            btnTerms.setBackgroundResource(R.color.colorPrimary);
            btnTerms.setTextColor(Color.parseColor("#ffffff"));

            cursor = dbManager.filteredFetchTerms(searched);
            checkResults(cursor);
            adapter = new SimpleCursorAdapter(getActivity(), R.layout.list_terms, cursor, fromTerm, to, 0);
        }
        adapter.notifyDataSetChanged();
        listViewSearcher.setAdapter(adapter);
        listViewSearcher.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                TextView t1 = view.findViewById(R.id.txtWordTerms);
                TextView t2 = view.findViewById(R.id.txtIdTerms);

                String topic = t1.getText().toString();
                String id = t2.getText().toString();

                Bundle bundle = new Bundle();
                bundle.putString("term",topic);
                bundle.putString("id",id);
                bundle.putString("search","yes");
                Fragment fragment1;
                if (searchTopic){
                    fragment1 = new TopicViewerFragment();
                }
                else {
                    fragment1 = new TermViewerFragment();
                    bundle.putBoolean("fromSearcher",true);
                }

                MainActivity activity = (MainActivity)getActivity();
                activity.fragment = fragment1;
                fragment1.setArguments(bundle);
                FragmentTransaction transaction = getFragmentManager().beginTransaction().addToBackStack("tag");
                transaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right,android.R.anim.slide_in_left,android.R.anim.slide_out_right );
                transaction.replace(R.id.frameLayout, fragment1).commit();
                activity.prevFragment = new SearcherFragmet();
                Log.i("hello","" + activity.fragment.getClass().toString());
            }
        });

        btnTerms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.searchTopics = false;
                searchTopic = false;
                btnTopics.setBackgroundResource(R.color.gray);
                btnTopics.setTextColor(Color.parseColor("#000000"));
                btnTerms.setBackgroundResource(R.color.colorPrimary);
                btnTerms.setTextColor(Color.parseColor("#ffffff"));

                cursor = dbManager.filteredFetchTerms(searched);
                checkResults(cursor);
                adapter = new SimpleCursorAdapter(getActivity(), R.layout.list_terms, cursor, fromTerm, to, 0);
                adapter.notifyDataSetChanged();
                listViewSearcher.setAdapter(adapter);
            }
        });
        btnTopics.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.searchTopics = true;
                searchTopic = true;
                btnTopics.setBackgroundResource(R.color.colorPrimary);
                btnTopics.setTextColor(Color.parseColor("#ffffff"));
                btnTerms.setBackgroundResource(R.color.gray);
                btnTerms.setTextColor(Color.parseColor("#000000"));

                cursor = dbManager.filteredFetchTopics(searched);
                checkResults(cursor);
                adapter = new SimpleCursorAdapter(getActivity(), R.layout.list_terms, cursor, fromTopic, to, 0);
                adapter.notifyDataSetChanged();
                listViewSearcher.setAdapter(adapter);
            }
        });

        return rootView;
    }
    public void checkResults(Cursor cursor){
        if (searched.equals("")){
            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)searchListContainer.getLayoutParams();
            params.height = 0;
            searchListContainer.setLayoutParams(params);
            txtSearcherQuery.setText("Start typing to search");
        }
        else {
            if (cursor.getCount() == 0){
                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)searchListContainer.getLayoutParams();
                params.height = 0;
                searchListContainer.setLayoutParams(params);
                txtSearcherQuery.setText("No match found");
            }
            else {
                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)searchListContainer.getLayoutParams();
                params.height = ViewGroup.LayoutParams.MATCH_PARENT;
                searchListContainer.setLayoutParams(params);
            }
        }
    }
}
