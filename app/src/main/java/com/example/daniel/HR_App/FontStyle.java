package com.example.daniel.HR_App;

/**
 * Created by Daniel on 26/03/2018.
 */

public enum FontStyle {
    FontStyle0(R.style.FontStyle_0,"0"),
    FontStyle1(R.style.FontStyle_1,"1"),
    FontStyle2(R.style.FontStyle_2,"2"),
    FontStyle3(R.style.FontStyle_3,"3"),
    FontStyle4(R.style.FontStyle_4,"4"),
    FontStyle5(R.style.FontStyle_5,"5"),
    FontStyle6(R.style.FontStyle_6,"6"),
    FontStyle7(R.style.FontStyle_7,"7"),
    FontStyle8(R.style.FontStyle_8,"8"),
    FontStyle9(R.style.FontStyle_9,"9"),
    FontStyle10(R.style.FontStyle_10,"10"),;

    private int resId;
    private String title;

    public int getResId() {
        return resId;
    }
    public String getTitle() {
        return title;
    }

    FontStyle(int resId, String title) {
        this.resId = resId;
        this.title = title;
    }
}
