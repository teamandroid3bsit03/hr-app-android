package com.example.daniel.HR_App;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Daniel on 27/03/2018.
 */

public class ExpTopicListFragment extends android.support.v4.app.Fragment{
    private ExpandableListView expListTopics;
    private ExpandableListAdapter expListAdapter;
    private List<String> listDataHeader;
    private HashMap<String,List<String>> listHashMap;
    MainActivity activity;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.expandable_topics, container, false);

        activity = (MainActivity) getActivity();
        expListTopics =(ExpandableListView)rootView.findViewById(R.id.expListView);

        initData();
        expListAdapter = new ExpandableListAdapter(getActivity().getApplicationContext(), listDataHeader, listHashMap);

        expListTopics.setAdapter(expListAdapter);

        if (!activity.parentGroup.isEmpty()){
            for (int x = 0; activity.parentGroup.size() > x; x++){
                expListTopics.expandGroup(activity.parentGroup.get(x));
            }
        }

        expListTopics.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                activity.parentGroup.add(groupPosition);
            }
        });
        expListTopics.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
            @Override
            public void onGroupCollapse(int groupPosition) {
                if (activity.parentGroup.contains(groupPosition)){
                    for (int x = 0; activity.parentGroup.size() > x; x++){
                        if (activity.parentGroup.get(x).equals(groupPosition)){
                            activity.parentGroup.remove(x);
                        }
                    }
                }
            }
        });
        expListTopics.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

                Bundle bundle = new Bundle();
                bundle.putInt("group",groupPosition);
                bundle.putInt("item",childPosition);

                Fragment fragment1 = new TopicViewerFragment();
                activity.fragment = fragment1;
                activity.prevFragment = new ExpTopicListFragment();
                activity.changeDrawerToBackButton();


                fragment1.setArguments(bundle);
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right,android.R.anim.slide_in_left,android.R.anim.slide_out_right );
                transaction.replace(R.id.frameLayout, fragment1).commit();

                return true;
            }
        });
        return rootView;
    }


    private void initData() {
        listDataHeader = new ArrayList<>();
        listHashMap = new HashMap<>();

        listDataHeader.add("ASSESSMENT FOR LEARNING");
        listDataHeader.add("CLASSROOM ENVIRONMENT AND CULTURE");
        listDataHeader.add("PURPOSE");
        listDataHeader.add("STUDENT ENGAGEMENT");

        List<String> assessment = new ArrayList<>();
        assessment.add("Checklist Guide for Effective Feedback");
        assessment.add("Classroom Assessment Strategies");
        assessment.add("Formative Assessment");
        assessment.add("Providing Effective Feedback");
        assessment.add("Sample CATs");
        assessment.add("Student Self-Assessment");

        List<String> cec = new ArrayList<>();
        cec.add("Communication Confident Authority");
        cec.add("Cooperative Reading Groups");
        cec.add("Cushioning Questions");
        cec.add("Learning Sheets");
        cec.add("Reading for Task");
        cec.add("Risk Language");
        cec.add("Setting Procedures and Expectations");
        cec.add("Truth Signs");

        List<String> pur = new ArrayList<>();
        pur.add("Checklist Guide to Sharing Learning Targets and Success Criteria");
        pur.add("Learning Target");
        pur.add("Performance Task");
        pur.add("Sharing Learning Targets and Success Criteria");
        pur.add("Success Criteria");

        List<String> se = new ArrayList<>();
        se.add("3-Step Interview");
        se.add("Analytic Teams");
        se.add("Buzz Groups");
        se.add("Drill Review Pairs");
        se.add("Face Off Game");
        se.add("Guided Reciprocal Peer Questioning");
        se.add("Jigsaw");
        se.add("Learning Cell");
        se.add("Mastery Learning Game");
        se.add("Note-Taking Pairs");
        se.add("Numbered Heads Together");
        se.add("Question, All Write");
        se.add("Questioning Strategies");
        se.add("Questioning Strategies2");
        se.add("Roundtable");
        se.add("Selecting Group Size");
        se.add("Selecting Members for Groups");
        se.add("Send a Problem");
        se.add("Structured Academic Controversies");
        se.add("Structured Problem Solving");
        se.add("Talking Chips");
        se.add("Test Taking Teams");
        se.add("Think Pair Share");
        se.add("Think Pair Square");
        se.add("Turn and Talk");
        se.add("Underexplain and Learning Pairs");
        se.add("Wait Time");

        listHashMap.put(listDataHeader.get(0),assessment);
        listHashMap.put(listDataHeader.get(1),cec);
        listHashMap.put(listDataHeader.get(2),pur);
        listHashMap.put(listDataHeader.get(3),se);
    }

}
